import SimpleHTTPServer
import SocketServer
import json
import hashlib
import re
from datetime import datetime
from io import BytesIO
from validate_message import validate_message

port = 8080
secret_key = "c0c10c15-fc13-451e-a95c-466d0f832eaf"

class ServerHandler(SimpleHTTPServer.SimpleHTTPRequestHandler):

    def do_POST(self):

		content_len = int(self.headers.getheader('content-length', 0))
		post_body = self.rfile.read(content_len)
		print "\nMessage received"
		print self.headers
		print post_body

		authentication = self.headers.getheader("authentication");

		message_validation = validate_message(post_body, authentication, secret_key);

		self.send_response(message_validation["status"])
		self.end_headers()
		
		response = BytesIO()
		response.write(str(message_validation))
		self.wfile.write(response.getvalue())


Handler = ServerHandler

httpd = SocketServer.TCPServer(("", port), Handler)

print "serving at port", port
httpd.serve_forever()
