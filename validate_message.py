import re
import json
import hashlib
from datetime import datetime

def validate_message(message, authentication, secret_key):

	responseList = []
	message_json = None

	if(not authentication):
		print ("Missing request header: \"Authentication\"")
		responseList.append("Missing request header: \"Authentication\"")
	else:
		dataMessageMD5 = hashlib.md5((message + secret_key).encode())
		dataMessageMD5Hash = dataMessageMD5.hexdigest()
		print "Calculated message MD5 hash: " + str(dataMessageMD5Hash)

		if dataMessageMD5Hash.lower() != authentication.lower():
			print("An exception occurred, Authentication doesn't match")
			responseList.append("authentication failed")

	try:
		message_json = json.loads(message)
	except:
		responseList.append("message not in json format")
		response = {
			"status": 400,
			"message": responseList
		}
		print response
		return response	
	
	# check if all fields of the message are present with valid values
	try:
		field = int(message_json["device_id"])
	except:
		print("An exception occurred, no field called \"device_id\"")
		responseList.append("missing field: \"device_id\"")
	
	try:
		message_date = message_json["message_date"]
		if message_date:
			try:
				date = datetime.strptime(message_date[:19], "%Y-%m-%dT%H:%M:%S")
			except:
				print("An exception occurred: invalid format for \"message_date\"")
				responseList.append("invalid format: \"message_date\"")		
	except:
		print("An exception occurred, no field called \"message_date\"")
		responseList.append("missing field: \"message_date\"")
	
	if "latitude" in message_json:
		if "longitude" not in message_json:
			print("missing field: \"longitude\"")
			responseList.append("missing field: \"longitude\"")

		field = message_json["latitude"]
		try:
			field = float(field)
		except:
			print("An exception occurred, field \"latitude\" is not a float")
			responseList.append("field \"latitude\" should be a float")

	if "longitude" in message_json:
		if "latitude" not in message_json:
			print("missing field: \"latitude\"")
			responseList.append("missing field: \"latitude\"")

		field = message_json["longitude"]
		try:
			field = float(field)
		except:
			print("An exception occurred, field \"longitude\" is not a float")
			responseList.append("field \"longitude\" should be a float")

	try:
		message_json = message_json["sensors"]
	except:
		print("An exception occurred, no field called \"sensors\"")
		responseList.append("missing field: \"sensors\"")
	
	try:
		for sensor_data in message_json:
			try:
				field = int(sensor_data["sensor_id"])
			except:
				print("An exception occurred, no field called \"sensor_id\"")
				responseList.append("missing field:  \"sensor_id\"")
			try:
				c = sensor_data["data"]
				for data in c:
					try:
						date = data["date"]
						if date:
							try:
								date = datetime.strptime(date[:19], "%Y-%m-%dT%H:%M:%S")
							except:
								print("An exception occurred: invalid format for \"date\"")
								responseList.append("invalid format: \"date\"")		
					except:
						print("An exception occurred, no field called \"date\"")
						responseList.append("missing field: \"date\"")

					try:
						field = float(data["value"])
					except:
						print("An exception occurred, no field called \"value\"")
						responseList.append("missing field: \"value\"")

			except:
				print("An exception occurred, no field called \"data\"")
				responseList.append("missing field: \"data\"")

	except:
		print("An exception occurred, no field called \"sensors\"")
		responseList.append("missing field: \"sensors\"")


	response = None
	if not responseList:
		response = {
			"status": 200,
			"message": ["message ok"]
		}
		print response
		return response

	else:
		response = {
			"status": 400,
			"message": responseList
		}
		print response
		return response
