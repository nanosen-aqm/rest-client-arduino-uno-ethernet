#include <Ethernet.h>
#include <DHT.h>;
#include <EthernetUdp.h>
#include <TimeLib.h>

// https://github.com/tzikis/ArduinoMD5
#include <MD5.h>

// https://github.com/maniacbug/MemoryFree
// #include <MemoryFree.h>

// Enter a MAC address for your controller below.
// Newer Ethernet shields have a MAC address printed on a sticker on the shield
byte mac[] = {
  0x66, 0x15, 0xF7, 0x7F, 0xCB, 0x37
};

// Initialize the Ethernet client library
EthernetClient ethClient;

#define DHTPIN 7     // what pin we're connected to
#define DHTTYPE DHT22   // DHT 22  (AM2302)
DHT dht(DHTPIN, DHTTYPE); //// Initialize DHT sensor for normal 16mhz Arduino


// the size of the array should be atleast 1 char longer than the actual key
const char secretKey[37] = "c0c10c15-fc13-451e-a95c-466d0f832eaf";

float temperature;
float humidity;

//*-------- NTP code ----------*/
unsigned int localPort = 51349;          // local port to listen for UDP packets

IPAddress timeServer(5, 135, 59, 152);  // ntp1.unixcraft.org. NTP server

const int timeZone = 1;                 // Central European Time

const int NTP_PACKET_SIZE = 48;         // NTP time stamp is in the first 48 bytes of the message

byte packetBuffer[NTP_PACKET_SIZE];    //buffer to hold incoming and outgoing packets

EthernetUDP Udp;                        // A UDP instance to let us send and receive packets over UDP




void setup() {
  // Open serial communications and wait for port to open:
  Serial.begin(9600);
  // this check is only needed on the Leonardo:
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  dht.begin();

  // start the Ethernet connection:
  if (Ethernet.begin(mac) == 0) {
    Serial.println("DHCPError");
    // no point in carrying on, so do nothing forevermore:
    for (;;)
      ;
  }
  // print your local IP address:
  printIPAddress();

  Udp.begin(localPort);
  setSyncProvider(getNtpTime);
}


void loop() {

  if (Ethernet.maintain() != 0) {
    Serial.println("DHCPError");
  }

  // Serial.print("freeMemory()=");
  // Serial.println(freeMemory());

  humidity = dht.readHumidity();
  temperature = dht.readTemperature();

  //humidity = 19.5;
  //temperature = 52.2;
  
  publishMessage("1", temperature);
  publishMessage("2", humidity);

  delay(2000);
}

void printIPAddress()
{
  Serial.print("My IP Address:");
  for (byte thisByte = 0; thisByte < 4; thisByte++) {
    // print the value of each byte of the IP address:
    Serial.print(Ethernet.localIP()[thisByte], DEC);
    Serial.print(".");
  }

  Serial.println();
}

void publishMessage(char sensorId[], float value) {
  char * date =  getDate();
  Serial.println(date);         
  
  char sensorValue[16] = "";

  // build json message
  char message[256] = "";
  strcat(message, "{\"device_id\":\"");
  strcat(message, "123456789");

  strcat(message, "\",\"message_date\":\"");
  strcat(message, date);

  strcat(message, "\",\"latitude\":\"");
  strcat(message, "-38.5714");

  strcat(message, "\",\"longitude\":\"");
  strcat(message, "-7.9135");

  strcat(message, "\",\"sensors\":[");
  strcat(message, "{\"sensor_id\":\"");
  strcat(message, sensorId);
  strcat(message, "\",\"data\":[{\"date\":\"");

  strcat(message, date);
  free(date);

  strcat(message, "\",\"value\":\"");

  dtostrf(value, 4, 2, sensorValue);
  strcat(message, sensorValue);
  strcat(message, "\"}]}");

  strcat(message, "]}");

  Serial.println(message);
  Serial.print(F("MessageSize"));
  Serial.println(strlen(message));

  //save message size to remove secretKey after message hash is done
  int messageSize = strlen(message);

  //append private key to message
  strcat(message, secretKey);
  Serial.println(message);

  //sign message together with private key
  unsigned char* md5_hash = MD5::make_hash(message);
  char *md5_hash_str = MD5::make_digest(md5_hash, 16);
  free(md5_hash);

  Serial.println(md5_hash_str);

  //mark the end of the message at before the private key
  //(remove the private key)
  message[messageSize] = '\0';

  // connect to http server and publish message
 
  if (ethClient.connect("192.168.1.72", 8080)) {  
  //if (ethClient.connect("193.137.120.244", 8080)) {

    Serial.println("Connected");

    int messageSize = strlen(message);

    // makes POST request
    // message headers
    ethClient.println("POST / HTTP/1.1");
    ethClient.println("Host: 193.137.120.242:8080");
    ethClient.println("User-Agent: Arduino/1.0");

    ethClient.println("Accept: */*");
    ethClient.println("Content-Type: application/json");
    //ethClient.println("Connection: close");
    ethClient.print("Authentication: ");
    ethClient.println(md5_hash_str);
    ethClient.print("Content-Length: ");
    ethClient.println(messageSize);

    ethClient.println();

    // message body
    ethClient.print(message);
    
    // wait for a response
    unsigned long startedWaiting = millis();
    while (!ethClient.available()) {

      // Time out after waiting for some time (2000ms)...
      if (millis() - startedWaiting > 2000) {
        Serial.println("Timed out...");
        break;
      }
      Serial.println("WaitingResponse");
    }

    // Read the HTTP response
    char statusLine[32] = "";
    boolean endOfStatusLine = false;

    Serial.println("HTTP Response: ");
    while (ethClient.available()) {
      char c = ethClient.read();

      if (!endOfStatusLine) {
        int statusLineLen = strlen(statusLine);

        // save the first line of the response
        // the first line contains de status code
        if (c != '\r') {
          statusLine[statusLineLen] = c;
          statusLine[statusLineLen + 1] = '\0';
        } else {
          endOfStatusLine = true;
        }

        // make sure that we don't write outside statusLine limits
        if (strlen(statusLine) > 32) {
          break;
        }
      }

      Serial.print(c);

    }

    Serial.println();
    Serial.print("StCode:");
    Serial.println(statusLine);

    // check if status code is 200 OK
    // we are considering HTTP 1.0 and 1.1
    // needs to be adjusted for HTTP version used by the server
    if (strcmp(statusLine, "HTTP/1.0 200 OK") != 0 &&
        strcmp(statusLine, "HTTP/1.1 200 OK") != 0) {
      Serial.print("Error: ");
      Serial.println(statusLine);
    }

  } else {
    Serial.println("NotConnected");
  }

  free(md5_hash_str);

}

/*-------- NTP code ----------*/
time_t getNtpTime()
{
  while (Udp.parsePacket() > 0) ; // discard any previously received packets
  Serial.println("Transmit NTP Request");
  sendNTPpacket(timeServer);
  uint32_t beginWait = millis();
  while (millis() - beginWait < 1500) {
    int size = Udp.parsePacket();
    if (size >= NTP_PACKET_SIZE) {
      Serial.println("Receive NTP Response");
      Udp.read(packetBuffer, NTP_PACKET_SIZE);  // read packet into the buffer
      unsigned long secsSince1900;
      // convert four bytes starting at location 40 to a long integer
      secsSince1900 =  (unsigned long)packetBuffer[40] << 24;
      secsSince1900 |= (unsigned long)packetBuffer[41] << 16;
      secsSince1900 |= (unsigned long)packetBuffer[42] << 8;
      secsSince1900 |= (unsigned long)packetBuffer[43];
      return secsSince1900 - 2208988800UL + timeZone * SECS_PER_HOUR;
    }
  }
  Serial.println("No NTP Response");
  return 0; // return 0 if unable to get the time
}

// send an NTP request to the time server at the given address
void sendNTPpacket(IPAddress &address)
{
  // set all bytes in the buffer to 0
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  // Initialize values needed to form NTP request
  // (see URL above for details on the packets)
  packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  packetBuffer[1] = 0;     // Stratum, or type of clock
  packetBuffer[2] = 6;     // Polling Interval
  packetBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12]  = 49;
  packetBuffer[13]  = 0x4E;
  packetBuffer[14]  = 49;
  packetBuffer[15]  = 52;
  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:                 
  Udp.beginPacket(address, 123); //NTP requests are to port 123
  Udp.write(packetBuffer, NTP_PACKET_SIZE);
  Udp.endPacket();
}

char * getDate(){
  char *dateString = (char *) malloc(sizeof(char)*32);
  strcpy (dateString, "");
 
  char dateBuf[4] = "";

  dtostrf(year(), 4, 0, dateBuf);
  strcat(dateString, dateBuf);
  strcat(dateString, "-");

  if(month() < 10){
    strcat(dateString, "0");
    dtostrf(month(), 1, 0, dateBuf);
  }else{
    dtostrf(month(), 2, 0, dateBuf);
  }
  strcat(dateString, dateBuf);
  strcat(dateString, "-");

  if(day() < 10){
    strcat(dateString, "0");
    dtostrf(day(), 1, 0, dateBuf);
  }else{
    dtostrf(day(), 2, 0, dateBuf);
  }
  strcat(dateString, dateBuf);
  strcat(dateString, "T");

  if(hour() < 10){
    strcat(dateString, "0");
    dtostrf(hour(), 1, 0, dateBuf);
  }else{
    dtostrf(hour(), 2, 0, dateBuf);
  }
  
  strcat(dateString, dateBuf);  
  strcat(dateString, ":");

 if(minute() < 10){
    strcat(dateString, "0");
    dtostrf(minute(), 1, 0, dateBuf);
  }else{
    dtostrf(minute(), 2, 0, dateBuf);
  }

  strcat(dateString, dateBuf);
  strcat(dateString, ":");

  if(second() < 10){
    strcat(dateString, "0");
    dtostrf(second(), 1, 0, dateBuf);
  }else{
    dtostrf(second(), 2, 0, dateBuf);
  }  
  strcat(dateString, dateBuf);

  // timeZone
  strcat(dateString, "+01:00");

  Serial.println(dateString);
  return dateString;
}
