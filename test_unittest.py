import unittest
from validate_message import validate_message

class TestValidateMessage(unittest.TestCase):

    secret_key = "nanosen12345678"

    def test_valid_message_with_location(self):

        message = '{"device_id":"123456789","message_date":"2019-07-10T13:46:59+00:00","latitude":"-38.5714","longitude":"-7.9135","sensors":[{"sensor_id":"123456782","data":[{"date":"2019-07-10T13:46:59+00:00","value":"62.10"}]}]}'
        auth = "78B4F0748444C0290347DB0FD90C7C09"

        expected = {"status":200, "message": ["message ok"]}
        result = validate_message(message, auth, self.secret_key)
       
        self.assertEquals(result, expected)


    def test_valid_message_wihtout_location(self):
        message = '{"device_id":"123456789","message_date":"2019-07-10T13:46:59+00:00","sensors":[{"sensor_id":"123456781","data":[{"date":"2019-07-10T13:46:59+00:00","value":"24.90"}]}]}'
        auth = "362459C376F2D72CA3FD494ABBBB631B"

        expected = {"status":200, "message": ["message ok"]}
        result = validate_message(message, auth, self.secret_key)
       
        self.assertEquals(result, expected)

        
    def test_message_with_missing_device_id(self):       
        message = '{"device-id":"123456789","message_date":"2019-07-10T13:46:59+00:00","latitude":"-38.5714","longitude":"-7.9135","sensors":[{"sensor_id":"123456781","data":[{"date":"2019-07-10T13:46:59+00:00","value":"24.90"}]}]}'
        auth = "EF7A5123B692A44933495B377F4FF607"

        expected = {"status":400, "message": ["missing field: \"device_id\""]}
        result = validate_message(message, auth, self.secret_key)
       
        self.assertEquals(result, expected)

    def test_message_only_with_longitude(self):
        message = '{"device_id":"123456789","message_date":"2019-07-10T13:46:59+00:00","longitude":"-7.9135","sensors":[{"sensor_id":"123456781","data":[{"date":"2019-07-10T13:46:59+00:00","value":"24.90"}]}]}'
        auth = "F0D51FDD3D07A471F60978171271D45C"

        expected = {"status":400, "message": ["missing field: \"latitude\""]}
        result = validate_message(message, auth, self.secret_key)
       
        self.assertEquals(result, expected)

    
    def test_message_only_with_latitude(self):
        message = '{"device_id":"123456789","message_date":"2019-07-10T13:46:59+00:00","latitude":"-38.5714","sensors":[{"sensor_id":"123456781","data":[{"date":"2019-07-10T13:46:59+00:00","value":"24.90"}]}]}'
        auth = "D75AEB870215DC7050F64E640C575A03"

        expected = {"status":400, "message": ["missing field: \"longitude\""]}
        result = validate_message(message, auth, self.secret_key)
       
        self.assertEquals(result, expected)

        
    def test_message_not_in_json_format(self):
        message = '"device-id":"123456789","message_date":"2019-07-10T13:46:59+00:00","latitude":"-38.5714","sensors":[{"sensor_id":"123456781","data":[{"date":"2019-07-10T13:46:59+00:00","value":"24.90"}]}]}'
        auth = "C5CB1188864FE734A76A755CDEEB1EAB"

        expected = {"status":400, "message": ["message not in json format"]}
        result = validate_message(message, auth, self.secret_key)
       
        self.assertEquals(result, expected)

        
    def test_missing_authentication_parameter(self):
        message = '{"device_id":"123456789","message_date":"2019-07-10T13:46:59+00:00","latitude":"-38.5714","longitude":"-7.9135","sensors":[{"sensor_id":"123456782","data":[{"date":"2019-07-10T13:46:59+00:00","value":"62.10"}]}]}'
        auth = None

        expected = {"status":400, "message": ["Missing request header: \"Authentication\""]}
        result = validate_message(message, auth, self.secret_key)
        #print result
       
        self.assertEquals(result, expected)

        
if __name__ == '__main__':
    unittest.main()
