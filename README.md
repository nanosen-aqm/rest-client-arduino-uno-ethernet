# NanoSenAQM RESTFul Client

This is an example implementation of a RESTFul client application to upload
sensor data to the NanoSenAQM cloud system using an Arduino Uno. It uses an
Arduino Uno micro controller coupled to a Ethernet Shield and connected to a
DHT22 temperature and humidity sensor module to collect test data.

The data upload mechanism consists of the following steps:

1. collect sensor data
1. build a JSON object with the sensor data
1. generate the signature of the message
1. create a HTTP Post request to upload data

**Note:** All Python scripts included in this project are made for Python 2.\*.

## Sensor data JSON object
Sensor data needs to be converted into a JSON object before being uploaded. This
JSON object allows to upload data from multiple sensors, and for each sensors
upload multiple values. The JSON object include the following properties:

| Key | Type | Description | Mandatory/Optional | Example |
|--|--|--|--|--|
| device_id | int | id of the device/cluster | Mandatory | 123456789 |
| message_date | string | date of the message (in ISO 8601 format)| Mandatory | 2019-07-10T13:46:59+00:00 |
| latitude | number | latitude of the device (in decimal degrees)| Optional |-38.5714 |
| longitude | number | longitude of the device (in decimal degrees)| Optional |-7.9135 |
| sensors | <sensor> array | array of sensor objects |Mandatory ||

### sensor object
The sensor object represents one sensor of the device. It is composed
of a sensor id and a list of data objects, where each data object represents a
data value. The sensor objects includes the following properties:

| Key | Type | Description | Mandatory/Optional |Example |
|--|--|--|--|--|
| sensor_id | int | id of the sensor | Mandatory |123456782 |
| data | <sensor> array | array of sensor data objects | Mandatory ||

### data object
The data object represents a data value of a sensor. This object includes the
sensor value and the acquisition date, as described bellow:

| Key | Type |Description | Mandatory/Optional | Example |
|--|--|--|--|--|
| value | number |sensor value to upload |  Mandatory | 54.20 |
| date | string | sensor value acquisition date (in ISO 8601 format)|  Mandatory  | 2019-07-10T13:46:59+00:00 |

### Example of a sensor data JSON object
The following example represents a RAW sensor data JSON object uploaded by the client:

```json
{"device_id":123456789,"message_date":"2019-07-10T13:46:59+00:00","latitude":"-38.5714","longitude":"-7.9135","sensors":[{"sensor_id":123456781,"data":[{"date":"2019-07-10T13:46:59+00:00","value":"24.90"}]}]}
```

To make it easier to read the RAW sensor data JSON object presented above, follows the same message after being formatted and prettified:

```json
{
  "device_id": 123456789,
  "message_date": "2019-07-10T13:46:59+00:00",
  "latitude": "-38.5714",
  "longitude": "-7.9135",
  "sensors": [
    {
      "sensor_id": 123456781,
      "data": [
        {
          "date": "2019-07-10T13:46:59+00:00",
          "value": "24.90"
        }
      ]
    }
  ]
}
```

## Signature
The security of the system is ensured by a mechanism based on Message
Authentication Code (MAC), where the message is signed together with secret key,
which is only known by the server and the client. In the context of the
NanoSenAQM project, to upload live sensor data using the RESTFul service, the
signature is the MD5 sum of the secret key appended to the sensor data JSON
object:

- signature = md5sum( sensor_data_JSON + secret )

where sensor_data_JSON is the sensor data JSON object represented as a string and secret_key is the secret key known only be the client
and the server.

## HTTP Post request
To upload data to the cloud system, the client should make a HTTP Post request with the sensor data JSON and the of signature of this object. The sensor data JSON object should be the body of the Post request. The signature of the JSON object should be uploaded as a request header with the name "Authentication".

**Note**: The sensor data JSON object representation **should be exactly the same** as the one used to generate the signature, otherwise the signature verification method will fail and the data will be discarded.

The request should include the following HTTP Headers, otherwise the data will be discarded:

| Name | Value | Example |
|--|--|--|
| Content-Type | application/json | application/json |
| Host | host:port | 193.137.120.244:8080 |
| User-Agent | client user agent | Arduino/1.0 |
| Accept | \*/\* | \*/\* |
| Content-Length | length of upload message | 212 |
| Authentication | signature of sensor data JSON object | c213182e462b51c147269b8c99b67545 |

The Content-Length header value should be the size of the upload message. If this value is incorrect or the header not included in the request, the data will be discarded.

### Response
The response of the HTTP Post request will have the status code 200 OK if the message is correctly received and validated by the cloud system.

If there is a problem processing the message, the HTTP response will have a different status code and include a description of the problem in the body of the response message.

# HTTP RESTful test server
This project includes a HTTP RESTful test server that can be used to test if a message sent by the client is valid. It checks if all keys are present in the JSON object and if the signature of the message is valid.

The server is configured to listen requests in `localhost` at port `8000`.

To use this server, you just need to run the `http_server.py` python script:

```shell
user@host:~$ python http_server.py
```

To use the RESTFul client with this server to check if the messages are ok, you should configure the client to connect to port `8000` of the host where the server is running. You should also change the secret key in the client to match the secret_key in the test server: `nanosen12345678`.

# Test JSON messages
If you want to test if a JSON message is valid for testing or debugging purposes, you can use one of the two following methods:

1. Use the `test_validate_message.py` Python scripts
1. Use curl to make a POST request to the RESTFul HTTP test server

## Using `test_validate_message.py`
The `test_validate_message.py` Python script validates a JSON message according to the predefined specifications. To use this script to test a JSON message, you just need to modify the script with the message that you want to test, the secret key used to sign message and invoke the validate funcion:

```python
secret_key = "nanosen12345678"

upload_message = '{"device_id":123456789,"message_date":"2019-07-10T13:46:59+00:00","latitude":"-38.5714","longitude":"-7.9135","sensors":[{"sensor_id":123456782,"data":[{"date":"2019-07-10T13:46:59+00:00","value":"62.10"}]}]}'
authentication= "78B4F0748444C0290347DB0FD90C7C09"

validate_message(upload_message, authentication, secret_key)
```
## Using curl
You can also use `curl` to make a request to the RESTFul HTTP server to validate a message. To do so, you should create file with the contents of the upload message, such as:

```json
{"device_id":123456789,"message_date":"2019-07-10T13:46:59+00:00","latitude":"-38.5714","longitude":"-7.9135","sensors":[{"sensor_id":123456782,"data":[{"date":"2019-07-10T13:46:59+00:00","value":"62.10"}]}]}
```

After that, you just need to run `curl` to make a HTTP POST Request with the proper headers, including the `Authentication` header, and the data in the post body:

```bash
user@host:~$ curl -v -H "Content-Type: application/json" -H "Authentication: 78B4F0748444C0290347DB0FD90C7C09" -X POST --data @params.json http://localhost:8000
```

where `params.json` is the file with the upload message and the value associated with the `Authentication` header is the signature of the message.

**Note:** If you want calculate the signature of the message found in the file `params.json` you should run the following command (valid in Linux):

```bash
user@host:~$ sh -c 'cat params.json; echo -n nanosen12345678' | md5sum
```
where `nanosen12345678` is the secret key used to sign the message.
