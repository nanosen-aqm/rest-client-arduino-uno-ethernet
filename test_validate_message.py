from validate_message import validate_message

secret_key = "nanosen12345678"

good_message1 = '{"device_id":"123456789","message_date":"2019-07-10T13:46:59+00:00","latitude":"-38.5714","longitude":"-7.9135","sensors":[{"sensor_id":"123456782","data":[{"date":"2019-07-10T13:46:59+00:00","value":"62.10"}]}]}'
auth_good1 = "78B4F0748444C0290347DB0FD90C7C09"

good_message2 = '{"device_id":"123456789","message_date":"2019-07-10T13:46:59+00:00","sensors":[{"sensor_id":"123456781","data":[{"date":"2019-07-10T13:46:59+00:00","value":"24.90"}]}]}'
auth_good2 = "362459C376F2D72CA3FD494ABBBB631B"

bad_message1 = '{"device-id":"123456789","message_date":"2019-07-10T13:46:59+00:00","latitude":"-38.5714","longitude":"-7.9135","sensors":[{"sensor_id":"123456781","data":[{"date":"2019-07-10T13:46:59+00:00","value":"24.90"}]}]}'
auth_bad1 = "EF7A5123B692A44933495B377F4FF607"

bad_message2 = '{"device_id":"123456789","message_date":"2019-07-10T13:46:59+00:00","longitude":"-7.9135","sensors":[{"sensor_id":"123456781","data":[{"date":"2019-07-10T13:46:59+00:00","value":"24.90"}]}]}'
auth_bad2 = "F0D51FDD3D07A471F60978171271D45C"

bad_message3 = '{"device-id":"123456789","message_date":"2019-07-10T13:46:59+00:00","latitude":"-38.5714","sensors":[{"sensor_id":"123456781","data":[{"date":"2019-07-10T13:46:59+00:00","value":"24.90"}]}]}'
auth_bad3 = "F7C46401133BCADF3C2BFE2C9FF8CA25"

bad_message4 = '"device-id":"123456789","message_date":"2019-07-10T13:46:59+00:00","latitude":"-38.5714","sensors":[{"sensor_id":"123456781","data":[{"date":"2019-07-10T13:46:59+00:00","value":"24.90"}]}]}'
auth_bad4 = "C5CB1188864FE734A76A755CDEEB1EAB"

bad_message5 = '"device-id":"123456789","message_date":"2019-07-10T13:46:59+00:00","latitude":"-38.5714","sensors":[{"sensor_id":"123456781","data":[{"date":"2019-07-10T13:46:59+00:00","value":"24.90"}]}]}'

validate_message(good_message1, auth_good1, secret_key)
validate_message(good_message2, auth_good2, secret_key)
validate_message(bad_message1, auth_bad1, secret_key)
validate_message(bad_message2, auth_bad2, secret_key)
validate_message(bad_message3, auth_bad3, secret_key)
validate_message(bad_message4, auth_bad4, secret_key)
validate_message(bad_message4, None, secret_key)
